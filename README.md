# homeeEpaperStatusDisplay

Displays MQTT based status message on a ePaper Display. You need a running MQTT broker like Mosquitto and homeeToMqtt. Both can be easily installed with homeean (https://homeean.de/buildscript).

The idea is based on the c't article "Ausdauernde Infotafel - Drahtloses T�rschild mit E-Paper-Display" (https://www.heise.de/ct/ausgabe/2018-2-Drahtloses-Tuerschild-mit-E-Paper-Display-3929847.html). It's using the basecamp library to manage basic IoT stuff (https://github.com/merlinschumacher/Basecamp).

# License
Licensed under GNU GENERAL PUBLIC LICENSE.

# Pinbelegung
- ePaper - ESP32
- 3,3v - 3,3v
- Gnd - Gnd
- CS - 5
- RESET - 16
- DC - 17
- DIN - 23
- CLK - 18
- BUSY - 4

# Installation
- Arduino installieren
- ESP32 Umgebung installieren: https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md
- Bibliotheken installieren:
        - Basecamp 0.1.8 incl. Dependencies (https://github.com/merlinschumacher/Basecamp):
            - ArduinoJson 5.13.2 (ArduinoJson 6.x not yet supported)
            - ESPAsyncWebServer, use older version: https://github.com/me-no-dev/ESPAsyncWebServer/tree/1078e9166b9af03073f2293edcbd8342764c6940 (copy manually to libraries folder)
            - Async MQTT Client (copy manually to libraries folder)
            - AsyncTCP (copy manually to libraries folder)
        - adafruit_gfx_library 1.3.1
        - GxEPD https://github.com/ZinggJM/GxEPD (copy manually to libraries folder)
        - Minigrafx https://github.com/ThingPulse/minigrafx (copy manually to libraries folder)
- Konfiguration im Quelltext:
    - DATA_LINES_EPAPER_AMOUNT setzen: Anzahl der Striche
        - Linien konfigurieren, z.B:
            -   dataLinesEpaper[0].startX = 0;
            -   dataLinesEpaper[0].startY = 0;
            -   dataLinesEpaper[0].endX = 399;
            -   dataLinesEpaper[0].endY = 0;
    - DYNAMIC_VALUES_SLOTS_AMOUNT setzen: Anzahl der dynamischen Anzeige Slots bei value exception
        - Dynamische Slots konfigurieren, z.B.
            -   dynamicValueSlotsEpaper[0].posTitleXDynamic = 7;
            -   dynamicValueSlotsEpaper[0].posTitleYDynamic = 168;
            -   dynamicValueSlotsEpaper[0].posValueXDynamic = 7;
            -   dynamicValueSlotsEpaper[0].posValueYDynamic = 188;
    - DATA_VALUES_EPAPER_AMOUNT setzen: Anzahl der Messpunkte
        - Messpunkte konfigurieren:
            -   dataValuesEpaper[0].title = "homee Status";     // Title to be shown
            -   dataValuesEpaper[0].isDynamicValue = false;     // Defines if value is always shown or only in case of deviation. Use defaultValue to define the case to don't show it
            -   dataValuesEpaper[0].posTitleXStatic = 7;        // Position Title X
            -   dataValuesEpaper[0].posTitleYStatic = 20;       // Position Title Y
            -   dataValuesEpaper[0].posValueXStatic = 7;        // Position Value X
            -   dataValuesEpaper[0].posValueYStatic = 40;       // Position Value Y
            -   dataValuesEpaper[0].type = 3;                   // 0 = show value, 1 = closed / open, 2 = off / on, 3 = homee modus, 4 = ok / alarm
            -   dataValuesEpaper[0].valuePrefix = "";           // optional prefix
            -   dataValuesEpaper[0].valuePostfix = "";          // optional postfix
            -   dataValuesEpaper[0].topicPath = "homee/human/homee(-1)/HomeeMode(1)";   // MQTT topic path to publishHuman path
            -   dataValuesEpaper[0].lastUpdated = 0;            // to be set to 0
        - Zus�tzliche Meldungen f�r die dynamischen Slots konfigurieren
            - Keine dynamischen Meldungen:
                -   dynamicAreaAllOkayText = "OK";
                -   dynamicAreaAllOkayTextStartX = 160;
                -   dynamicAreaAllOkayTextStartY = 230;
            -   Mehr dynamische Meldungen als Slots:
                -   dynamicAreaMoreItemsText = "...";
                -   dynamicAreaMoreItemsTextStartX = 376;
                -   dynamicAreaMoreItemsTextStartY = 290;
- Kompilieren und auf den ESP32 laden.
- Mit dem auf dem Display angezeigten WLAN verbinden, angegebene Adresse im Browser aufrufen, Konfiguration vornehmen.
- Nach der Konfiguration startet der ESP32 neu und sollte mit der Anzeige starten. Ggf. dauert es einige Minuten bis homee2mqtt alle Werte erneut published. 
